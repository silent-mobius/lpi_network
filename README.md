# Network Protocol Essentials

## Overview

Welcome to the Network Protocol Essentials! This course is for anyone who wants to learn more about what network protocols are and which protocols are most widely used today. This course provides a basic introduction to network protocols and is designed to be accessible to anyone who wants to learn more about the topic. We'll start by answering the question, “What is a network protocol?” and then discuss some of the conceptual models of various types of network traffic. Next, our focus will shift to the protocols themselves. We'll learn about all of the main protocols that network communications and the internet are built on. We'll also cover several protocols that we all use on a daily basis

-   [Fundamentals](./00_fundamentals/README.md)
-   [Subnetting](01_subnet/README.md)
-   [Routing](./02_route/README.md)
-   [Trouble Shooting](03_tb/README.md)
