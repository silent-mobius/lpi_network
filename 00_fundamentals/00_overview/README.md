# Network protocols overview


the deifinition of word "protocol" might have various meanings, yet in context of our corse it can be explained as __set of rules__.
When we talk about network protocols, it means the formal standards and policies by which 2 or more devices are talking.

Early example of network protocol are telegram radio and many more.

### Some bullet point to consider:
-   Network communication happens between various layers of network protocol.
-   Each layer of network communication is responsible for passing the information on to next layer.
-   Data transfered between layers is known as Protocol Data Unit (PDU).
-   Layers allow better trouble shooting.
-   As protocol are created or improved, older protocols become outdated and unsecure.


### Terminology

-   __LAN__: Local Area Network.
-   __WAN__: Wide Area Network.
-   __MAN__: Metropolitan Area Network.
-   __ISP__: Internet Service Provider.
-   __NAT__: Network Address Translation.
-   __Firewall__: HW or SW, that enforces what type of network traffic is or is not allowed.
-   __Router__: Device that provides communication between diffent LAN's.
-   __Switch__: Device that provides comminitcation in local are network.
-   __Hub__: Device that provides communication in LAN, yet, it doesn’t know anything about the computers
that are connected to each of its ports. So when a computer connected to the hub sends a packet to a computer that’s connected to another port, the hub sends a duplicate copy of the packet to all its ports. In contrast, a switch knows which computer is connected to each of its ports. As a result, when
a switch receives a packet intended for a particular computer, it sends the packet only to the port that the recipient is connected to.
-   __NIC__: Network Interface Card.
-   __Port__: Can be described as net socket, but in our context is seen as endpoint for transfered data.
-   __Packet__: Basic unit of data transfered over the network.
-   __Node__: Device that is connected to the network.
-   __Client__ (computer): The device that end users use to access the resources of the network.
-   __Server__ (computer): The device that provide shared resources, such as disk storage and printers, as well as network services, such as e-mail and Internet access.

### common Media connection types:

-   __UTP__  Twisted pair cabling is a type of wiring in which two conductors of a single circuit are twisted together for the purposes of improving electromagnetic compatibility. Compared to a single conductor or an untwisted balanced pair, a twisted pair reduces electromagnetic radiation from the pair and crosstalk between neighboring pairs and improves rejection of external electromagnetic interference.

-   __STP__ :

-   __FDDI__: Fiber Distributed Data Interface is a standard for data transmission in a local area network. It uses optical fiber as its standard underlying physical medium.

-   __Coax__: Coaxial cable is a type of transmission line, used to carry high frequency electrical signals with low losses. It is used in such applications as telephone trunklines, broadband internet networking cables, high speed computer data busses, cable television signals, and connecting radio transmitters and receivers to their antennas. It differs from other shielded cables because the dimensions of the cable and connectors are controlled to give a precise, constant conductor spacing, which is needed for it to function efficiently as a transmission line. 

### Topologies

The term network topology refers to the shape of how the computers and other network components are connected to each other. the most known topologies are:

-   __Bus Topology__: In this type of topology, nodes are strung together in a line. Bus topology is commonly used
for LANs. The key to understanding how a bus topology works is to think of the entire network as a single cable, with each node “tapping” into the cable so that it can listen in on the packets being sent over that cable.
In a bus topology, every node on the network can see every packet that’s sent on the cable. Each node looks at each packet to determine whether the packet is intended for it. If so, the node claims the packet. If not, the node
ignores the packet. This way, each computer can respond to data sent to itand ignore data sent to other computers on the network.
If the cable in a bus network breaks, the network is effectively divided into
two networks. Nodes on either side of the break can continue to communi-
cate with each other, but data can’t span the gap between the networks, so
nodes on opposite sides of the break can’t communicate with each other.

-   __Star Topology__: Here, each network node is connected to a central device called a hub or a switch. Star topologies are also commonly
used with LANs. If a cable in a star network breaks, only the node connected to that cable is
isolated from the network. The other nodes can continue to operate without
interruption — unless, of course, the node that’s isolated because of the
break happens to be the file server.

-   __Ring__: A third type of network topology is called a ring. packets are sent around the circle from computer to computer. Each computer looks at each packet to decide whether the packet was intended
for it. If not, the packet is passed on to the next computer in the ring ears ago, ring topologies were common in LANs, as two popular networking technologies used rings: ARCNET and Token Ring. ARCNET is still used for certain applications such as factory automation, but is rarely used in business networks. Token Ring is a popular network technology for IBM midrange computers. Although plenty of Token Ring networks are still in existence, not many new networks use Token Ring any more. Ring topology was also used by FDDI, one of the first types of fiber-optic network connections. FDDI has given way to more efficient fiber-optic techniques,however. So ring networks have all but vanished from business networks.x

-   __Mesh__: The last type of network topology, has multiple connections between each of the nodes on the network. The advantage of a mesh topology is that if one cable breaks, the network can use an alternative route to deliver its packets. Mesh networks are not very practical in a LAN setting. For example, to network eight computers in a mesh topology, each computer would have to have seven network interface cards, and 28 cables would be required to connect each computer to the seven other computers in the network. Obviously, this scheme isn’t very scalable.
However, mesh networks are common for metropolitan or wide area networks. These networks use devices called routers to route packets from network to network. For reliability and performance reasons, routers are usually arranged
in a way that provides multiple paths between any two nodes on the network in a mesh-like arrangement.


### Standards:

-   ANSI (American National Standards Institute) www.ansi.org
-   IEEE (Institute of Electrical and Electronic Engineers) www.ieee.org
-   ISO (International Organization for Standardization) www.iso.org
-   IETF (Internet Engineering Task Force) www.ietf.org
-   W3C (World Wide Web Consortium) www.w3c.org
