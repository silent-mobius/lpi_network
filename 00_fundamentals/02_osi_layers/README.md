# OSI Layers

These are the 7 Layers os OSI Model



-   [Physical](./01_physical.md)
-   [Data Link](./02_data_link.md)
-   [Network](./03_net.md)
-   [Transport](./04_transport.md)
-   [Session](./05_session.md)
-   [Presentation](./06_present.md)
-   [Application](./07_app.md)
