# OSI and TCP


-   [OSI](./00_osi.md)
-   [TCP](./01_tcp.md)
-   [UDP](./02_udp.md)

![Grapical Representation](../../.img/ositcp.jpeg)
